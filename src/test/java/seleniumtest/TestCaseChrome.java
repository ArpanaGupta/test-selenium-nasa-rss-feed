package seleniumtest;


import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestCaseChrome {
	  private WebDriver driver;
	  private String testURL;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();

	  @Before
	  public void setUp() throws Exception {
		testURL = System.getProperty("testURL");
		System.setProperty("webdriver.chrome.driver","src\\test\\resources\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();  
		options.setHeadless(true);
	    driver = new ChromeDriver(options);	    
	    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	  }

	  @Test
	  public void testCaseChrome() throws Exception {
	    driver.get("http://" +testURL+ ":8080/nasarssfeed/");
	    driver.findElement(By.linkText("Education")).click();
	    //driver.findElement(By.linkText("Image Of the Day")).click();
	    driver.findElement(By.linkText("Breaking News")).click();
	  }

	  @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}
